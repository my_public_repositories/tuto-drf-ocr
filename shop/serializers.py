from rest_framework.serializers import ModelSerializer, SerializerMethodField, ValidationError
from shop.models import Category, Product, Article


class ArticleSerializer(ModelSerializer):
    class Meta:
        model = Article
        fields = ["id", "name", "date_created", "date_updated", "product"]


class ArticleListSerializer(ModelSerializer):
    articles = SerializerMethodField

    class Meta:
        model = Article
        fields = ["id", "name", "date_created", "date_updated", "product", "price"]

    def validate_price(self, value):
        if value < 1:
            raise ValidationError("Price must be > 1")
        return value

    def validate(self, data):
        if not data["product"].active:
            raise ValidationError("Product must be active")
        return data


class ProductDetailSerializer(ModelSerializer):
    articles = SerializerMethodField()

    class Meta:
        model = Product
        fields = ["id", "name", "date_created", "date_updated", "category", "articles"]

    def get_articles(self, instance):
        queryset = instance.articles.filter(active=True)
        serializer = ArticleSerializer(queryset, many=True)
        return serializer.data


class ProductListSerializer(ModelSerializer):
    # articles = SerializerMethodField()

    class Meta:
        model = Product
        fields = ["id", "name", "date_created", "date_updated", "category", "ecoscore"]


class CategoryDetailSerializer(ModelSerializer):
    # products = ProductSerializer(many=True)
    products = SerializerMethodField()

    class Meta:
        model = Category
        fields = ["id", "name", "date_created", "date_updated", "products"]

    def get_products(self, instance):
        queryset = instance.products.filter(active=True)
        serializer = ProductListSerializer(queryset, many=True)
        return serializer.data


class CategoryListSerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ["id", "name", "date_created", "date_updated", "description"]

    def validate_name(self, value):
        if Category.objects.filter(name=value).exists():
            raise ValidationError("Category already exists")
        return value

    def validate(self, data):
        if data["name"] not in data["description"]:
            raise ValidationError("Name must be in description")
        return data
